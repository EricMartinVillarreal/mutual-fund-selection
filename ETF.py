from config import expected_market_return, rf_rate


class ETF(object):

    def __init__(self,
                 ticker,
                 expense_ratio,
                 current_share_price,
                 beta,
                 expected_equity_return,
                 dividend_yield,
                 actual_return):

        self.ticker = ticker
        self.expense_ratio = expense_ratio
        self.current_share_price = current_share_price
        self.beta = beta
        self.expected_equity_return = expected_equity_return
        self.dividend_yield = dividend_yield
        self.actual_return = actual_return

    def __str__(self):
        return self.ticker

    def __repr__(self):
        return self.ticker

    ticker = ""
    expense_ratio = 0
    current_share_price = 0
    beta = 0
    expected_equity_return = 0
    dividend_yield = 0
    actual_return = 0


def make_etf(ticker, expense_ratio, current_share_price, beta, dividend_yield):
    """
    :param ticker: Ticker Symbol of ETF
    :param expense_ratio: Expense Ratio of ETF
    :param current_share_price: Current price of a share of the ETF
    :param beta: 3Y Monthly beta of the ETF
    :param dividend_yield: 12 Month trailing dividend yield
    :return: object of new ETF
    """
    # CAPM to solve for expected_equity_return
    expected_equity_return = rf_rate + beta*(expected_market_return - rf_rate)

    # ETF expenses get taken out of the share price
    actual_return = expected_equity_return - expense_ratio

    etf = ETF(ticker,
              expense_ratio,
              current_share_price,
              beta,
              expected_equity_return,
              dividend_yield,
              actual_return)
    global etf_list
    etf_list.append(etf)

    return etf


# Creates empty etf list
etf_list = []

# SPDR S&P 500 ETF
SPY = make_etf('SPY', .0009, 279.14, 1, 0.0189)

# Vanguard S&P 500 Growth ETF
VOOG = make_etf('VOOG', .0015, 150.68, 1.02, 0.0125)

# Vanguard S&P 500 ETF
VOO = make_etf('VOO', 0.0004, 256.61, 1, .0191)

# Vanguard S&P 500 Value ETF
VOOV = make_etf('VOOV', 0.0015, 108.97, .98, .0244)

# Vanguard High Dividend Yield ETF
VYM = make_etf('VYM', 0.0008, 85.83, .87, .0320)

# Vanguard Dividend Appreciation ETF
VIG = make_etf('VIG', 0.0008, 109.05, .92, .0196)

# SPDR S&P Dividend ETF
SDY = make_etf('SDY', 0.0035, 99.63, .85, .0257)
