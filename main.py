from config import years_until_retirement
from Selection import get_net_asset_value_etf, get_net_asset_value_mutual_fund
from M_Fund import mutual_fund_list
from ETF import etf_list
import operator


def etf_performance(years):
    """
    :param years: years until retirement
    :return: performance of all ETFs
    """
    etf_performance_dict = {}

    # Useful for printing or returning sorted_etf_performance_dict
    for fund in etf_list:
        etf_performance_dict[fund.ticker] = round(get_net_asset_value_etf(fund, years))
    sorted_etf_performance_dict = sorted(etf_performance_dict.items(),
                                         key=operator.itemgetter(1),
                                         reverse=True)

    return etf_performance_dict


def mutual_fund_performance(years):
    """
    :param years: years until retirement
    :return: performance of all mutual funds
    """
    mutual_fund_performance_dict = {}

    # Useful for  printing or returning sorted_mutual_fund_performance_dict
    for fund in mutual_fund_list:
        mutual_fund_performance_dict[fund.ticker] = round(get_net_asset_value_mutual_fund(fund, years))
    sorted_mutual_fund_performance_dict = sorted(mutual_fund_performance_dict.items(),
                                                 key=operator.itemgetter(1),
                                                 reverse=True)

    return mutual_fund_performance_dict


def combined_fund_performance(years):
    """
    :param years: years until retirement
    :return: performance of ETFs and mutual funds
    """
    combined_fund_dict = {**etf_performance(years), **mutual_fund_performance(years)}

    sorted_combined_fund_dict = sorted(combined_fund_dict.items(),
                                       key=operator.itemgetter(1),
                                       reverse=True)
    return sorted_combined_fund_dict

