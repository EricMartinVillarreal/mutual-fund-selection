from config import rf_rate, max_income, starting_income, income_scaling


def get_income(year):
    """
    :param year: takes in current year
    :return: income for that year
    """

    income_by_year = {}
    for y in range(80):
        inflation = (rf_rate + 1) ** y

        if y <= (income_scaling - 1):
            income_by_year[y] = round((((y * (max_income - starting_income))
                                        / income_scaling) + starting_income) * inflation)
        else:
            income_by_year[y] = round(max_income * inflation)
    return income_by_year[year]


def get_retirement_contribution(year, percent_saved):
    """
    :param year: Takes in current year
    :param percent_saved: Percentage of income put towards retirement
    :return: Retirement contribution for that year
    """
    retirement_contribution = get_income(year) * percent_saved
    return retirement_contribution


def get_two_week_contribution(year, percent_saved):
    """
    :param year: Takes in current year
    :param percent_saved: Percentage of income put towards retirement each year
    :return: Salary contribution to retirement every two weeks
    """
    two_week_contribution = get_retirement_contribution(year, percent_saved) / 26
    return two_week_contribution

