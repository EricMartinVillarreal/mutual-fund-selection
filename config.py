initial_investment = 5000               # Initial investment in Year 0
years_until_retirement = 40             # Years to save until retirement

expected_market_return = .05            # Expected return of S&P 500
rf_rate = 0.02654                       # Risk-free rate

commission_cost = 7                     # Brokerage commission cost per trade; only  on ETFs

mf_annual_fee = 20                      # Annual fee if account is under some amount
mf_minimum_account_balance = 10000      # Minimum amount that must be reached in account for fee not to be charged

etf_annual_fee = 20                     # Annual fee if account is under some amount
etf_minimum_account_balance = 10000     # Minimum amount that must be reached in account for fee not to be charged

income_savings_percent = .15            # The percentage of yearly income saved for retirement
max_income = 150000                     # Ending income before inflation
starting_income = 20000                 # Beginning income before inflation
income_scaling = 25                     # Years until max income is reached
