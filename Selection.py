from config import *
from income import get_two_week_contribution


# Convert to quarterly is deprecated. Intended for dividend yield, but the complicated thing below solves it.
def convert_to_quarterly_rate(rate):
    """
    :param rate: annual rate of interest
    :return: quarterly rate of interest for compounding purposes
    """
    quarterly_rate = ((1 + rate)**(1/4)) - 1

    return quarterly_rate


def convert_to_daily_rate(rate):
    """
    :param rate: annual rate of interest
    :return: daily rate of interest for compounding purposes
        """
    daily_rate = ((1 + rate) ** (1 / 365)) - 1

    return daily_rate


def get_net_asset_value_etf(etf, years):
    """
    :param etf:
    :param years:
    :return:
    """

    yearly_fee = 0
    yearly_dividends = 0

    initial_share_price = etf.current_share_price
    share_price = etf.current_share_price
    shares_owned = initial_investment // etf.current_share_price
    unused_cash = initial_investment % etf.current_share_price

    daily_actual_return = convert_to_daily_rate(etf.actual_return)

    # Complicated way of having dividends increase with time, but still equal the annual dividend yield
    dividend_yield_adjustment = (1 + daily_actual_return) ** 92 \
        + (1 + daily_actual_return) ** 183 \
        + (1 + daily_actual_return) ** 273 \
        + (1 + daily_actual_return) ** 365
    quarterly_dividend_yield = (etf.dividend_yield * (share_price*((1 + daily_actual_return)**365))) \
        / (initial_share_price * dividend_yield_adjustment)

    for y in range(years):
        for i in range(365):
            # Increase share price daily
            share_price *= (1 + daily_actual_return)

            # Pay out salary every two week, and buy shares of etf if possible
            if (i + 1) % 14 == 0:
                unused_cash += get_two_week_contribution(y, income_savings_percent)
                if unused_cash >= (share_price + commission_cost):
                    unused_cash -= commission_cost
                    shares_owned += (unused_cash // share_price)
                    unused_cash -= ((unused_cash // share_price) * share_price)

            # Pay out dividends quarterly
            if i == 91 or i == 182 or i == 272 or i == 364:
                unused_cash += (share_price * quarterly_dividend_yield * shares_owned)
                yearly_dividends += (share_price * quarterly_dividend_yield * shares_owned)

                # Charges fee at end of year if account is less than some minimum balance
                if i == 364 and ((share_price * shares_owned) + unused_cash) < etf_minimum_account_balance:
                    yearly_fee = etf_annual_fee
                    # Takes fee out of unused cash in account if possible, otherwise takes out of shares
                    if unused_cash > yearly_fee:
                        unused_cash -= yearly_fee
                    else:
                        shares_owned -= yearly_fee / share_price
                else:
                    yearly_fee = 0

            # Buy share if sitting cash is greater than share price
            if unused_cash >= (share_price + commission_cost):
                unused_cash -= commission_cost
                shares_owned += (unused_cash // share_price)
                unused_cash -= ((unused_cash // share_price) * share_price)

    value_of_shares = share_price * shares_owned
    net_asset_value = value_of_shares + unused_cash

    return net_asset_value


def get_net_asset_value_mutual_fund(mutual_fund, years):
    """
    :param mutual_fund:
    :param years:
    :return:
    """
    yearly_dividends = 0

    initial_share_price = mutual_fund.current_share_price
    share_price = mutual_fund.current_share_price
    shares_owned = initial_investment / mutual_fund.current_share_price

    daily_actual_return = convert_to_daily_rate(mutual_fund.actual_return)

    # Complicated way of having dividends increase with time, but still equal the annual dividend yield
    dividend_yield_adjustment = (1 + daily_actual_return) ** 92 \
        + (1 + daily_actual_return) ** 183 \
        + (1 + daily_actual_return) ** 273 \
        + (1 + daily_actual_return) ** 365
    quarterly_dividend_yield = (mutual_fund.dividend_yield * (share_price * ((1 + daily_actual_return) ** 365))) \
        / (initial_share_price * dividend_yield_adjustment)

    for y in range(years):
        for i in range(365):
            # Increase share price daily
            share_price *= (1 + daily_actual_return)

            # Pay out salary every two week, and buy shares of mutual fund
            if (i + 1) % 14 == 0:
                shares_owned += get_two_week_contribution(y, income_savings_percent) / share_price

            # Pay out dividends quarterly, and buy shares of mutual fund
            if i == 91 or i == 182 or i == 272 or i == 364:
                shares_owned += (share_price * quarterly_dividend_yield * shares_owned) / share_price
                yearly_dividends += (share_price * quarterly_dividend_yield * shares_owned)

                # Charges fee at end of year if account is less than some minimum balance
                if i == 364 and (share_price * shares_owned) < mf_minimum_account_balance:
                    yearly_fee = mf_annual_fee
                    shares_owned -= yearly_fee / share_price
                else:
                    yearly_fee = 0

    value_of_shares = share_price * shares_owned
    net_asset_value = value_of_shares

    return net_asset_value
