from config import rf_rate, expected_market_return


class MutualFund(object):

    def __init__(self, ticker, expense_ratio, current_share_price, beta, expected_equity_return, dividend_yield,
                 actual_return):
        self.ticker = ticker
        self.expense_ratio = expense_ratio
        self.current_share_price = current_share_price
        self.beta = beta
        self.expected_equity_return = expected_equity_return
        self.dividend_yield = dividend_yield
        self.actual_return = actual_return

    def __str__(self):
        return self.ticker

    def __repr__(self):
        return self.ticker

    ticker = ""
    expense_ratio = 0
    current_share_price = 0
    beta = 0
    expected_equity_return = 0
    dividend_yield = 0
    actual_return = 0


def make_mutual_fund(ticker, expense_ratio, current_share_price, beta, dividend_yield):
    """
    :param ticker: Ticker Symbol of mutual fund
    :param expense_ratio: Expense Ratio of the mutual fund
    :param current_share_price: Current price of a share of the mutual fund
    :param beta: 3Y Monthly beta of the mutual fund
    :param dividend_yield: 12 Month trailing dividend yield
    :return: object of new mutual fund
    """
    # CAPM to solve for expected_equity_return
    expected_equity_return = rf_rate + beta*(expected_market_return - rf_rate)

    # Mutual fund returns after expenses
    actual_return = expected_equity_return - expense_ratio

    mutual_fund = MutualFund(ticker,
                             expense_ratio,
                             current_share_price,
                             beta,
                             expected_equity_return,
                             dividend_yield,
                             actual_return)
    global mutual_fund_list
    mutual_fund_list.append(mutual_fund)

    return mutual_fund


# Creates empty mutual fund list
mutual_fund_list = []

# Vanguard 500 Index Fund Admiral Shares
VFIAX = make_mutual_fund('VFIAX', 0.0004, 258.60, 1, 0.0190)

# Vanguard Dividend Appreciation Index Fund Admiral Shares
VDADX = make_mutual_fund('VDADX', 0.0008, 29.59, .92, .0195)

# Vanguard Growth Index Admiral Shares
VIGAX = make_mutual_fund('VIGAX', 0.0005, 78.23, 1.07, 0.012)

# Fidelity 500 Index
FXAIX = make_mutual_fund('FXAIX', 0.0002, 97.32, 1, 0.0191)

# Schwab S&P 500 Index
SWPPX = make_mutual_fund('SWPPX', 0.0002, 42.8, 1, 0.0203)

# Parnassus Endeavor Investor
PARWX = make_mutual_fund('PARWX', 0.0092, 34.35, 1.3, 0.0146)

# Vanguard High Dividend Yield Admiral Shares
VHYAX = make_mutual_fund('VHYAX', 0.0008, 25.87, .87, 0.0320)



