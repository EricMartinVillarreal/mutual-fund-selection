Config.py contains editable variables.

ETF.py and M_fund.py contain funds to select from.

main.py contains main script to provide net asset value of all funds.

plot.py contains data visualization functions.