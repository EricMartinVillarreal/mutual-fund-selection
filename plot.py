from main import *
import matplotlib.pyplot as plt
import numpy as np


def nav_line_plot(x1, x2):
    """
    :param x1: starting x value for time horizon
    :param x2: ending x value for time horizon
    :return: a plot of all funds' net asset values over time
    """

    plt.xlim(0, x2-x1)
    plt.xticks([0, (x2-x1)], [x1, x2])
    combined_list_of_funds = etf_list + mutual_fund_list

    # key is fund ticker, value is a list of yearly returns
    fund_yearly_return_list = {}
    for fund in combined_list_of_funds:
        fund_yearly_return_list[str(fund)] = []

    for year in range(x1, x2 + 1):
        for entry in combined_fund_performance(year):
            fund_yearly_return_list[entry[0]].append(entry[1])

    for fund in fund_yearly_return_list:
        x = fund_yearly_return_list[fund]
        plt.plot(x, label=fund)

    plt.xlabel('Years')
    plt.ylabel('Net Asset Value')
    plt.legend(loc='best')

    return plt.show()


def nav_growth_line_plot(x1, x2):
    """
    :param x1: starting x value for time horizon
    :param x2: ending x value for time horizon
    :return: plot of annual percentage growth in nav for all funds
    """
    plt.xlim(0, x2-x1-1)
    plt.xticks([0, (x2 - x1 - 1)], [x1+1, x2])

    combined_list_of_funds = etf_list + mutual_fund_list

    # key is fund ticker, value is a list of yearly returns
    fund_yearly_return_list = {}
    for fund in combined_list_of_funds:
        fund_yearly_return_list[str(fund)] = []

    for year in range(x1, x2+1):
        for entry in combined_fund_performance(year):
            fund_yearly_return_list[entry[0]].append(entry[1])

    for fund in fund_yearly_return_list:
        t = fund_yearly_return_list[fund].copy()
        for i in range(len(fund_yearly_return_list[fund])):
            if i > 0:
                fund_yearly_return_list[fund][i] = ((t[i] - t[i-1]) / t[i-1])*100
            if i + 1 == len(fund_yearly_return_list[fund]):
                fund_yearly_return_list[fund].pop(0)
                x = fund_yearly_return_list[fund]
                plt.plot(x, label=fund)

    plt.xlabel('Years')
    plt.ylabel('Annual Growth in Net Asset Value')
    plt.legend(loc='best')
    return plt.show()

def return_bar_graph():
    """
    :return: a bar graph of the returns of all funds
    """

    combined_list_of_funds = etf_list + mutual_fund_list
    combined_list_of_funds.sort(key=lambda x: (x.actual_return + x.dividend_yield), reverse=True)
    for fund in combined_list_of_funds:
        x = str(fund)
        y = fund.actual_return + fund.dividend_yield
        plt.bar(x, y)
    plt.xticks(fontsize='x-small')
    plt.xlabel('Funds')
    plt.ylabel('Total Yearly Return')
    plt.autoscale(enable=True, axis='both', tight='False')
    return plt.show()

# Uncomment these to use
#return_bar_graph()
#nav_line_plot(0, 40)
#nav_growth_line_plot(0, 40)
